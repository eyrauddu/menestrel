# Comment j'ai installé *menestrel*

## Objectif: lire de la musique avec une interface agréable

## Matériel: 
* un Raspberry Pi, 
* un serveur NAS où stocker la musique, 
* des enceintes, 
* un keypad USB

## Software:
* Raspbian comme distribution de base
* mpd (Music Player Daemon) et mpc (le client) pour gérer la musique
* lirc et irexec pour effectuer des actions quand une touche est pressée


De ce que j'avais lu, MPD peut utiliser le protocole UPnP pour obtenir
liste de chansons depuis un serveur UPnP. Mais soit mon NAS est pas un
bon serveur pour ce protocole, soit j'ai pas bien su le mettre en
place. En tous cas, c'était très lent et pas du tout utilisable. Donc
j'ai décidé de monter le répertoire directement sur le Raspberry et
configurer MPD pour lire dans le répertoire.

Ce dépôt contient mes fichiers de configuration tels qu'installés chez
moi. 

## Etape 0 (Important)
Avoir une bibliothèque de musique en bon état (bien
tagguée en particulier)

## Étape 1: Monter le répertoire de musique sur le raspberry

Mon NAS s'appelle inertiel, et utilise Avahi, j'ai mis en place un
montage NFS en ajoutant cette ligne dans /etc/fstab (à adapter selon
le partage):

```
inertiel.local:/mnt/HD/ /mnt/inertiel nfs auto,rw  0  2
```

Pour que le boot se passe bien, j'ai dû spécifier à systemd que mpd doit
attendre que le montage NFS soit terminé. J'ai modifié le fichier 
`/lib/systemd/system/mpd.service`, et à la ligne `After=`, j'ai rajouté 
`remote-fs.target`.


## Étape 2: Configurer mpd

Le fichier de configuration s'appelle `/etc/mpd.conf`. La doc est
plutôt bien faite: https://www.musicpd.org/doc/html/user.html

À changer notablement:

### Mettre en place les bons répertoires

Ici aussi, à adapter au cas de chacun.

```
music_directory                "/mnt/inertiel/Musique/"
playlist_directory              "/mnt/inertiel/Musique/Playlists"
```

### Configuration du buffer audio

J'avais fait ça quand la connection était en WiFi, ça a amélioré mais
c'était pas parfait, il est peut-être possible de mieux fixer ces
paramètres.

```
audio_buffer_size "16000"
buffer_before_play "10%"
```

### Configuration administrative

Je me souviens plus si j'ai dû faire `adduser mpd mpd` ou pas.

```
user "mpd"
zeroconf_enabled "yes"
zeroconf_name    "menestrel"
```

### Configuration du plugin

```
database {
   plugin "simple"
   path "/var/lib/mpd/db"
}
```

## Étape 4: tester si ça marche
À partir d'ici, il devrait être possible de faire de la musique.
`mpc play` depuis menestrel doit marcher. Et meme `mpc -h
menestrel.local play` devrait marcher depuis un ordi local.

Comme clients desktop, j'aime bien `gmpc`.

Installer `MPDroid` sur un smartphone permet aussi de contrôler la
musique. Par contre j'ai l'impression qu'il ne sait pas utiliser
avahi, du coup `menestrel.local` n'a jamais marché, il a fallu le
configurer avec l'adresse IP en dur.

## Étape 5: Configurer LIRC

Je me souviens avoir galéré un peu pour mettre ça en place. La doc de
LIRC (http://www.lirc.org/html/) n'est pas facile à lire parce qu'elle
est faite pour plein de cas très différents, et trouver le sien n'est
pas trivial.

Il existe `lirc-config-tool` et `lirc-make-devinput` qui peuvent
aider. En gros il faut que lirc sache quel code de touche associer à
quelle valeur reçue depuis le clavier.

J'ai édité le fichier /etc/lirc/lirc_options.conf pour mettre la ligne

```
device          = /dev/input/by-id/usb-04d9_1203-event-kbd
```

Qui correspond à mon keypad USB. 

Et ensuite, j'ai écrit mes petits scripts maison (disponibles dans
`music-scripts/`), et j'ai modifié le fichier `irexec.lircrc` pour
connecter la bonne touche à la bonne commande. Exemple pour la touche
*Play/Pause* :

```
begin 
prog=irexec
button=KEY_KPENTER
config=mpc toggle
end
```


Pour qu'irexec se lance au démarrage : `systemctl start irexec.service`

## Étape 6: Bonus 

J'ai installé `espeak`, téléchargé une voix de `mbrola` (le binaire :
http://www.tcts.fpms.ac.be/synthesis/mbrola/bin/raspberri_pi/mbrola.tgz)
(la voix :
http://www.tcts.fpms.ac.be/synthesis/mbrola/dba/fr1/fr1-990204.zip) et
installé comme ça : 

``` 
sudo cp mbrola /usr/local/bin 
sudo mkdir -p /usr/share/mbrola/fr1 
sudo cp fr1/fr1 /usr/share/mbrola/fr1/
```

Puis j'ai mis en place la commande:

```
begin
prog=irexec
button=KEY_ESC
config=mpc pause; mpc -f "%artist%" |head -n1 |sudo -u pi espeak -stdin  -s 145 -v mb/mb-fr1 2>/dev/null; mpc play
end
```

Pour avoir une touche "Qui chante ça ?" qui lui fait dire le nom de
l'artiste.


