#!/bin/bash

dir=$(dirname $0)

if [[ $1 = "" ]]
then
  exit
fi

if ${dir}/is-special-key.sh; then 
	${dir}/add-current-song-to-playlist "$1"
	${dir}/rm-special-key.sh
	exit
fi


echo "Playing playlist $*"
mpc clear
for i in "$@"; do 
  mpc load "$i"
done
mpc random off
mpc shuffle
mpc play
