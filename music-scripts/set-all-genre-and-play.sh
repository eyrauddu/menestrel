#!/bin/bash

if [[ $1 = "" ]]
then
  exit
fi

echo "Playing genre $*"
mpc clear
for i in "$@"; do 
  mpc find genre "$i" | mpc add
done
mpc random off
mpc shuffle
mpc play
