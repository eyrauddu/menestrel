#!/bin/bash

if [[ $1 = "" ]]
then
  exit
fi

echo "Playing albums like $*"
mpc clear
for i in "$@"; do 
  mpc searchadd album "$i" 
done
mpc random on
mpc play
