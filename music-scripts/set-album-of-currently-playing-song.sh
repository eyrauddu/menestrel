#!/bin/bash

cur=$(mpc current)
echo "$cur"

artist=$(echo "$cur" |cut -d- -f1)
title=$(echo "$cur" | cut -d- -f2)

artist=${artist%% }
title=${title## }
echo "$artist"
echo "$title"

album=$(mpc list album artist "$artist" title "$title" | head -n 1)
echo "$album"

mpc crop
mpc find album "$album" | mpc add
mpc random off

