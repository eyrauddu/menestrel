#!/bin/bash

if [[ $1 = "" ]]
then
  exit
fi

dir=$(dirname $0)

# list=$({ for i in "$@"; do mpc list album genre "$i"; done } )
# echo "$list"
album=$({ for i in "$@"; do mpc list album genre "$i"; done } | ${dir}/pick-random.sh)
echo "Playing $album of genre $@"
mpc clear
mpc find album "$album" | mpc add
mpc random off
mpc play
